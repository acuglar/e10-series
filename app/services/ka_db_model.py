from ..services import conn_cur


class KaSeriesTable:
    FIELDNAMES = ['id', 'serie', 'seasons', 'released_date', 'genre', 'imdb_rating']

    def _create_table(self) -> None:
        conn, cur = conn_cur()

        cur.execute(
            """
                CREATE TABLE IF NOT EXISTS ka_series
                    (
                        id BIGSERIAL PRIMARY KEY, 
                        serie VARCHAR(100) NOT NULL UNIQUE,
                        seasons INTEGER NOT NULL,
                        released_date DATE NOT NULL,
                        genre VARCHAR(50) NOT NULL,
                        imdb_rating FLOAT NOT NULL
                    )
            """
        )

        conn.commit()
        cur.close()
        conn.close()


    def insert_values(self, data: dict) -> None:
        conn, cur = conn_cur()
        self._create_table()

        cur.execute(
        """
            INSERT INTO ka_series
                (serie, seasons, released_date, genre, imdb_rating)
            VALUES
                (%(serie)s, %(seasons)s, %(released_date)s, %(genre)s, %(imdb_rating)s)  
            RETURNING *          
        """, data,
        )

        query = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()


    def get_data(self) -> list[dict]:
        conn, cur = conn_cur()

        cur.execute('SELECT * FROM ka_series')

        query = cur.fetchall()

        conn.commit()
        cur.close()
        conn.close()

        data = [dict(zip(self.FIELDNAMES, row)) for row in query]
        
        for d in data:
            d.update({'released_date': d['released_date'].strftime('%d/%m/%Y')})
        
        return {'data': data}


    def get_serie(self):
        conn, cur = conn_cur()

        cur.execute(
            """
                SELECT * FROM ka_series
                WHERE id = %(id)s
            """
            )

        query = cur.fetchone()

        conn.commit()
        cur.close()
        conn.close()

        data = dict(zip(self.FIELDNAMES, query))

        return data
        

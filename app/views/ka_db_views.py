from flask import Flask, request, jsonify
from http import HTTPStatus
from ..services import KaSeriesTable



def ka_series_database(app):

    @app.route('/series', methods=['POST'])
    def create():
        series = KaSeriesTable()

        data = request.get_json()
        data['serie'] = data['serie'].title()
        data['genre'] = data['genre'].title()
        
        series.insert_values(data)          

        return data, HTTPStatus.CREATED

    
    @app.route('/series', methods=['GET'])
    def series():
        series = KaSeriesTable()
        
        data = series.get_data()
        return jsonify(data), HTTPStatus.OK

                
    @app.route('/series/<int:serie_id>', methods=['GET'])
    def select_by_id(serie_id):
        series = KaSeriesTable()
        
        data = series.get_serie(serie_id)
        
        if data:
            return data, HTTPStatus.OK
        else:
            return {}, HTTPStatus.NOT_FOUND

